
public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView leftIcon = findViewById(R.id.left_icon);
        ImageView rightIcon = findViewById(R.id.right_icon);

        TextView title = findViewById(R.id.toolbar_title);

        leftIcon.setOnClickListener(new View.onClickListener(){

            @Override
            public void onClick(View view){
                Toast.makeText(MainActivity.this, "L Clicked.",Toast.LENGTH_SHORT).show();
            }
        });
        
        rightIcon.setOnClickListener(new View.onClickListener(){
        
            @Override
            public void onClick(View view){
                Toast.makeText(MainActivity.this, "R Clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }
}




public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ImageView leftIcon = findViewById(R.id.leftIcon);
		ImageView rightIcon = findViewById(R.id.rightIcon);

		TextView title = findViewById(R.id.toolbar_title);

		leftIcon.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View view){
				Toast.makeText(MainActivity.this, "Left Icon Clicked Successfully", Toast.LENGTH_SHORT).show();
			}
		});

		rightIcon.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View view){
				Toast.makeText(MainActivity.this, "Right Icon Clicked Successfully", Toast.LENGTH_SHORT).show();
			}
		});

		title.setText("Any Text");
	}
}